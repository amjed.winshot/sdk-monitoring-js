import { Monitoring } from '../index';
describe('Define Monitoring', () => {
  let instance: Monitoring;
  beforeAll(() => {
    instance = new Monitoring({
      API_KEY: '',
      API_URL: '',
      enabled: true,
      environment: 'DEVELOPMENT',
    });
  });
  test('Define  instance', () => {
    expect(instance.info).toBeDefined();
  });
});
