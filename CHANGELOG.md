# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.31](///compare/v0.0.30...v0.0.31) (2024-01-04)

### [0.0.30](///compare/v0.0.29...v0.0.30) (2024-01-04)

### [0.0.29](///compare/v0.0.28...v0.0.29) (2023-12-21)


### Bug Fixes

* add nonCircularClone func a148dc8

### [0.0.28](///compare/v0.0.27...v0.0.28) (2023-11-08)

### [0.0.27](///compare/v0.0.26...v0.0.27) (2023-10-30)

### [0.0.26](///compare/v0.0.25...v0.0.26) (2023-10-28)

### [0.0.25](///compare/v0.0.24...v0.0.25) (2023-10-27)


### Bug Fixes

* isonline c356e3b

### [0.0.24](///compare/v0.0.23...v0.0.24) (2023-10-27)


### Bug Fixes

* handleQueue ebcd304

### [0.0.23](///compare/v0.0.22...v0.0.23) (2023-10-27)

### [0.0.22](///compare/v0.0.21...v0.0.22) (2023-10-27)

### [0.0.21](///compare/v0.0.20...v0.0.21) (2023-10-25)

### [0.0.20](///compare/v0.0.19...v0.0.20) (2023-10-25)

### [0.0.19](///compare/v0.0.18...v0.0.19) (2023-10-25)

### [0.0.18](///compare/v0.0.17...v0.0.18) (2023-10-25)

### [0.0.17](///compare/v0.0.16...v0.0.17) (2023-10-25)

### [0.0.16](///compare/v0.0.15...v0.0.16) (2023-10-25)

### [0.0.15](///compare/v0.0.14...v0.0.15) (2023-10-24)

### [0.0.14](///compare/v0.0.13...v0.0.14) (2023-10-24)

### [0.0.13](///compare/v0.0.12...v0.0.13) (2023-10-24)

### [0.0.12](///compare/v0.0.11...v0.0.12) (2023-10-24)

### [0.0.11](///compare/v0.0.10...v0.0.11) (2023-10-24)


### Bug Fixes

* **rn:** process is undefined c8bf537

### [0.0.10](///compare/v0.0.9...v0.0.10) (2023-10-24)


### Bug Fixes

* update 42b04af

### [0.0.9](///compare/v0.0.8...v0.0.9) (2023-10-24)


### Bug Fixes

* remove rm f719b53

### [0.0.8](///compare/v0.0.7...v0.0.8) (2023-10-24)

### [0.0.7](///compare/v0.0.6...v0.0.7) (2023-10-24)

### [0.0.6](///compare/v0.0.5...v0.0.6) (2023-10-24)

### [0.0.5](///compare/v0.0.4...v0.0.5) (2023-10-24)

### [0.0.4](///compare/v0.0.3...v0.0.4) (2023-10-24)


### Bug Fixes

* remove imports 23f3f91

### [0.0.3](///compare/v0.0.2...v0.0.3) (2023-10-24)


### Features

* add RNative e50c54b

### 0.0.2 (2023-10-24)

### 0.0.1 (2022-12-13)
