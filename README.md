### Monitoring SDK

Unified typescript SDK library that contains all API calls for monitoring.

### Installation
1. Make sure you have axios installed 
2. install command
```bash
# using yarn
yarn add @gofiled/sdk-monitoring-js

# using npm
npm install @gofiled/sdk-monitoring-js
```

### Usage

#### Create a new instance

```javascript

import { Monitoring } from '@gofiled/sdk-monitoring-js';

// Create an instance
const monitoringInstance = new Monitoring({
      API_KEY: '<Your_API_kEY_FOR_A_PROJECT>',
      API_URL: '<BASE_HTTP_URL>',
      enabled: true,
      envirement: 'production',
});
```

#### Properties

| Property           | Description                                                               | Default Value                |
|--------------------|---------------------------------------------------------------------------|------------------------------|
| `enabled`          | Is the monitoring service is enabled                                      | `required`                   |
| `API_KEY`          | Your_API_kEY_FOR_A_PROJECT                                                | `required`                   |
| `API_URL`          | `BASE_HTTP_URL`                                                           | `required`                   |
| `envirement`       | `PRODUCTION` or `DEVELOPMENT` or `TEST`                                   | `required`                   |
| `captureUncaught`  | if set to true, we capture any errors globally                            | default to `true`            |
| `enableDebug`      | For debugging perpuses                                                    | default to false             |
| `customLogger`     | For debugging perpuses                                                    | default value to `undefined` |
| `transformPayload` | A function that transform the payload if you want to add extra attributes | default value to `undefined` |

#### Functions

| Function        | Description                         |
|-----------------|-------------------------------------|
| `configureUser` | Configure the current user          |
| `removeUser`    | Set the current user to `undefined` |
| `info`          | Send `info payload` to the server   |
| `debug`         | Send `debug payload` to the server  |
| `warn`          | Send `warn payload` to the server   |
| `error`         | Send `error payload` to the server  |
| `setIsOnline`   | Manually change the `Online status` |