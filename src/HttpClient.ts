import axios, { AxiosInstance } from 'axios';
import { MonitoringConfig, PostItemPayload } from './types';

declare module 'axios' {
  export interface AxiosInstance {
    request<T = any>(config: AxiosRequestConfig): Promise<T>;
    get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    head<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
    post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
    put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
    patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
  }
}
function doNothing(...args: any[]) {
  //
}
export default class HttpClient {
  private readonly instance: AxiosInstance;
  readonly config: MonitoringConfig;
  readonly logger: (...args: any[]) => void;
  constructor(config: MonitoringConfig) {
    this.config = config;
    this.instance = axios.create({
      baseURL: this.config.API_URL,
    });
    this.logger = this.config.customLogger || doNothing;
    this._initializeResponseInterceptor();
  }

  private _initializeResponseInterceptor() {
    this.instance.interceptors.request.use((config) => {
      if (this.config.API_KEY) {
        config.headers['Authorization'] = this.config.API_KEY;
      }
      return config;
    });
    this.instance.interceptors.response.use((response) => response.data);
  }

  postItem(payload: PostItemPayload, { onError }: { onError?: (error: any, payload: PostItemPayload) => void } = {}) {
    const finalPayload = this.config.transformPayload?.(payload) || payload;

    if (this.config.enableDebug) {
      this.logger(finalPayload);
    }
    const defaultpayload: Partial<PostItemPayload> = {
      environment: this.config.environment,
    };
    const payloadToSend = { ...defaultpayload, ...finalPayload };
    this.instance.post('/logs', payloadToSend).catch((err) => {
      if (this.config.enableDebug) {
        this.logger(err);
      }
      if (onError) {
        onError(err, payloadToSend);
      }
    });
  }
}
