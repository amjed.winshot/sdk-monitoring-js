/**
 * The function `getBrowserType` returns the type of browser based on the user agent string.
 * @returns The function `getBrowserType` returns the type of browser based on the user agent string.
 */
export function getBrowserType(): Record<string, string> {
  if (!window || !window.navigator) {
    return {} as const;
  }
  const test = (regexp: RegExp) => {
    return regexp.test(navigator.userAgent);
  };
  const userAgent = navigator.userAgent;

  if (test(/opr\//i) || !!window.opr) {
    return {
      browser: 'Opera',
      userAgent,
    };
  } else if (test(/edg/i)) {
    return {
      browser: 'Microsoft Edge',
      userAgent,
    };
  } else if (test(/chrome|chromium|crios/i)) {
    return {
      browser: 'Google Chrome',
      userAgent,
    };
  } else if (test(/firefox|fxios/i)) {
    return {
      browser: 'Mozilla Firefox',
      userAgent,
    };
  } else if (test(/safari/i)) {
    return {
      browser: 'Apple Safari',
      userAgent,
    };
  } else if (test(/trident/i)) {
    return {
      browser: 'Internet Explorer',
      userAgent,
    };
  } else if (test(/ucbrowser/i)) {
    return {
      browser: 'UC Browser',
      userAgent,
    };
  } else if (test(/samsungbrowser/i)) {
    return {
      browser: 'Samsung Internet',
      userAgent,
    };
  } else {
    return {
      browser: 'Unknown browser',
      userAgent,
    };
  }
}

/**
 * The `nonCircularClone` function is a TypeScript function that creates a deep clone of an object,
 * handling circular references by removing them and replacing them with a string indicating the type
 * of the circular reference.
 * @param {object} obj - The `obj` parameter is an object that you want to clone without any circular
 * references.
 * @returns The function `nonCircularClone` returns a clone of the input object `obj`, with any
 * circular references removed.
 */
export function nonCircularClone(obj: object) {
  const seen: object[] = [obj];

  function clone(obj: any, seen: any[]) {
    let value,
      name,
      newSeen,
      result: Record<string, any> | string = {};

    try {
      for (name in obj) {
        value = obj[name];

        if (value && typeof value === 'object') {
          if (seen.includes(value)) {
            result[name] = 'Removed circular reference: ' + typeof value;
          } else {
            newSeen = seen.slice();
            newSeen.push(value);
            result[name] = clone(value, newSeen);
          }
          continue;
        }

        result[name] = value;
      }
    } catch (e) {
      let errorMessage = 'Failed cloning custom data: ';
      if (e && typeof e === 'object' && 'message' in e) {
        errorMessage += e.message;
      }
      result = errorMessage;
    }
    return result;
  }
  return clone(obj, seen);
}
