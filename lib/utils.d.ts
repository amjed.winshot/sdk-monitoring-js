/**
 * The function `getBrowserType` returns the type of browser based on the user agent string.
 * @returns The function `getBrowserType` returns the type of browser based on the user agent string.
 */
export declare function getBrowserType(): Record<string, string>;
/**
 * The `nonCircularClone` function is a TypeScript function that creates a deep clone of an object,
 * handling circular references by removing them and replacing them with a string indicating the type
 * of the circular reference.
 * @param {object} obj - The `obj` parameter is an object that you want to clone without any circular
 * references.
 * @returns The function `nonCircularClone` returns a clone of the input object `obj`, with any
 * circular references removed.
 */
export declare function nonCircularClone(obj: object): string | Record<string, any>;
