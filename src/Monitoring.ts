import { isAxiosError } from 'axios';
import HttpClient from './HttpClient';
import { MonitoringConfig, PostItemPayload } from './types';
import { getBrowserType, nonCircularClone } from './utils';
declare global {
  interface Window {
    __DEV__?: boolean;
    opr?: unknown;
  }
}
type PostItemPayloadArg = Pick<PostItemPayload, 'title' | 'user' | 'extra'> & {
  content?: any;
};
function doNothing(...args: any[]) {
  //
}
export default class Monitoring {
  private readonly config: MonitoringConfig;
  private readonly httpInstance: HttpClient;
  private user?: PostItemPayloadArg['user'];
  private readonly isBrowser = typeof window !== 'undefined' && window?.document !== undefined;
  private readonly isReactNative = typeof window !== 'undefined' && typeof window.__DEV__ === 'boolean';
  private readonly isNodeServer = typeof process !== 'undefined' && process?.release?.name === 'node';
  private customLogger = doNothing;
  // TODO
  // private readonly isReactNative = typeof global !== 'undefined' && global?.nativeModule;
  private queue: PostItemPayload[] = [];
  private isOnline = true;
  constructor(config: MonitoringConfig) {
    const defaultConfig: Partial<MonitoringConfig> = {
      captureUncaught: true,
      enableDebug: false,
    };
    this.config = { ...defaultConfig, ...config };
    if (this.config.enableDebug && this.config.customLogger) {
      this.customLogger = this.config.customLogger;
    }
    this.httpInstance = new HttpClient(this.config);

    this.init();
  }

  private init() {
    // TODO
    if (this.isReactNative) {
      return this.handleReactNative();
    }
    if (this.isNodeServer) {
      return this.handleNodeServer();
    }
    if (this.isBrowser) {
      return this.handleBrowser();
    }
  }

  //  react native specific
  private handleReactNative() {
    // TODO
  }

  //  node server specific
  private handleNodeServer() {
    if (this.config.captureUncaught && this.config.enabled) {
      process.on('uncaughtException', (error) => {
        this.error({
          title: 'uncaughtException',
          content: error,
        });
      });
      process.on('unhandledRejection', (reason, promise) => {
        this.error({
          title: 'Unhandled Rejection:',
          content: reason,
        });
      });
    }
  }

  private handleBrowser() {
    if (this.config.captureUncaught && this.config.enabled) {
      window.addEventListener('error', (event) => {
        event.preventDefault();

        const {
          message = 'Unknown error',
          filename = 'Unknown source',
          lineno = 'Unknown line number',
          colno = 'Unknown column number',
          error = 'Unknown error object',
        } = event;

        const title = `Uncaught promise rejection: ${message}`;

        let errorMessage = `An error occurred:\nMessage: ${message}\nSource: ${filename}\nLine: ${lineno}\nColumn: ${colno}`;
        if (typeof error === 'object' && error !== null) {
          errorMessage += `\nError: ${JSON.stringify(nonCircularClone(error))}`;
        }

        this.error({
          title,
          content: errorMessage,
          extra: {
            url: window.location.href,
          },
        });
      });
    }

    window.addEventListener('online', () => {
      this.isOnline = true;
      this.customLogger('back online');
      this.handleQueue();
    });
    window.addEventListener('offline', () => {
      this.customLogger('going offline');
      this.isOnline = false;
    });
  }

  /**
   * The handleQueue function processes items in a queue by sending them as HTTP POST requests.
   */
  private handleQueue() {
    if (!this.config.enabled || !this.isOnline) {
      return;
    }

    while (this.queue.length > 0) {
      this.httpInstance.postItem(this.queue.shift()!);
    }
  }
  configureUser(user: PostItemPayloadArg['user']) {
    this.user = user;
  }
  removeUser() {
    this.user = undefined;
  }
  info(payload: PostItemPayloadArg) {
    this.log('INFO', payload);
  }
  debug(payload: PostItemPayloadArg) {
    this.log('DEBUG', payload);
  }
  warn(payload: PostItemPayloadArg) {
    this.log('WARNING', payload);
  }
  error(payload: PostItemPayloadArg) {
    this.log('ERROR', payload);
  }
  setIsOnline(isOnline: boolean) {
    this.isOnline = isOnline;
    if (this.isOnline) {
      this.handleQueue();
    }
  }
  private log(level: PostItemPayload['level'], payload: PostItemPayloadArg) {
    if (!this.config.enabled) {
      return;
    }
    const error = this.stringifyError(payload.content);

    const finalPayload: PostItemPayload = {
      title: payload.title,
      content: error,
      level,
      environment: this.config.environment,
      user: payload.user || this.user,
      extra: payload.extra,
    };
    if (this.isBrowser) {
      const browserInfo = getBrowserType();
      finalPayload.extra = {
        ...(finalPayload.extra || {}),
        ...browserInfo,
      };
    }
    if (!this.isOnline) {
      this.customLogger('offline wating to be online', finalPayload);
      return this.queue.push(finalPayload);
    }
    //
    this.customLogger('sending log', finalPayload);
    this.sendPayload(finalPayload);
  }

  private sendPayload(payload: PostItemPayload) {
    this.httpInstance.postItem(payload, {
      onError: (error) => {
        if (isAxiosError(error) && error.message === 'Network Error') {
          this.queue.push(payload);
        }
      },
    });
  }

  private stringifyError(error: unknown) {
    if (typeof error === 'string') {
      return error;
    }
    if (error instanceof Error) {
      return JSON.stringify({
        name: error.name,
        message: error.message,
        stack: error.stack,
      });
    }
    if (typeof error === 'object' && error !== null) {
      try {
        return JSON.stringify(nonCircularClone(error));
      } catch (error) {
        return `Error: Unable to stringify error object. ${error?.toString()}`;
      }
    }
    return `Error: Unknown error type - ${error?.toString()}`;
  }
}
