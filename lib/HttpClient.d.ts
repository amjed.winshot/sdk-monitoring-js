import { MonitoringConfig, PostItemPayload } from './types';
declare module 'axios' {
    interface AxiosInstance {
        request<T = any>(config: AxiosRequestConfig): Promise<T>;
        get<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
        delete<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
        head<T = any>(url: string, config?: AxiosRequestConfig): Promise<T>;
        post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
        put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
        patch<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T>;
    }
}
export default class HttpClient {
    private readonly instance;
    readonly config: MonitoringConfig;
    readonly logger: (...args: any[]) => void;
    constructor(config: MonitoringConfig);
    private _initializeResponseInterceptor;
    postItem(payload: PostItemPayload, { onError }?: {
        onError?: (error: any, payload: PostItemPayload) => void;
    }): void;
}
