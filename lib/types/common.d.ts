export type MonitoringConfig = {
    API_KEY: string;
    API_URL: string;
    enabled: boolean;
    environment: 'PRODUCTION' | 'DEVELOPMENT' | 'TEST' | 'LOCAL';
    captureUncaught?: boolean;
    enableDebug?: boolean;
    customLogger?: (...args: any[]) => void;
    transformPayload?: (payload: PostItemPayload) => PostItemPayload;
};
export type PostItemPayload = {
    level: 'DEBUG' | 'INFO' | 'WARNING' | 'ERROR';
    environment: MonitoringConfig['environment'];
    title: string;
    content?: string;
    extra?: Record<string, string>;
    user?: {
        _id?: string;
        fullName?: string;
        identity?: string;
        company?: string;
    };
};
