import { MonitoringConfig, PostItemPayload } from './types';
declare global {
    interface Window {
        __DEV__?: boolean;
        opr?: unknown;
    }
}
type PostItemPayloadArg = Pick<PostItemPayload, 'title' | 'user' | 'extra'> & {
    content?: any;
};
export default class Monitoring {
    private readonly config;
    private readonly httpInstance;
    private user?;
    private readonly isBrowser;
    private readonly isReactNative;
    private readonly isNodeServer;
    private customLogger;
    private queue;
    private isOnline;
    constructor(config: MonitoringConfig);
    private init;
    private handleReactNative;
    private handleNodeServer;
    private handleBrowser;
    /**
     * The handleQueue function processes items in a queue by sending them as HTTP POST requests.
     */
    private handleQueue;
    configureUser(user: PostItemPayloadArg['user']): void;
    removeUser(): void;
    info(payload: PostItemPayloadArg): void;
    debug(payload: PostItemPayloadArg): void;
    warn(payload: PostItemPayloadArg): void;
    error(payload: PostItemPayloadArg): void;
    setIsOnline(isOnline: boolean): void;
    private log;
    private sendPayload;
    private stringifyError;
}
export {};
